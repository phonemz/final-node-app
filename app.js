const express = require('express');
const mongoose = require('mongoose');

const app = express();

// middleware
app.use(express.static('public'));

// view engine
app.set('view engine', 'ejs');

const authRoutes = require("./route/authRoute");
app.use(authRoutes);

// database connection
const dbURI =
        "mongodb+srv://phone:v99glfLuRTeMCwt9@final-exam.jbhhw0a.mongodb.net/final-exam";
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(3000))
  .catch((err) => console.log(err));

// routes
app.get('/', (req, res) => res.render('home'));

const cookieParser = require("cookie-parser");
app.use(cookieParser());
