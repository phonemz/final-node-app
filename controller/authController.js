module.exports.login_get = (req, res) => {
        res.render("login");
};

module.exports.login_post = async (req, res) => {
        res.send("new login"); //to test it
};

const User = require("../model/user");
module.exports.login_post = async (req, res) => {
        const { email, password } = req.body;
        try {
                const user = await User.create({ email, password }); // create a user basically a async task that’s ywe choose to wait till out user gets created and store in user.
                res.status(201).json(user);
        } catch (err) {
                console.log(err);
                res.status(400).send("error, user not created");
        }
};

const jwt = require("jsonwebtoken");
// handle errors
const handleErrors = (err) => {
        console.log(err.message, err.code);
        let errors = { email: "", password: "" };
        // duplicate email error
        if (err.code === 11000) {
                errors.email = "that email is already registered";
                return errors;
        }
        // validation errors
        if (err.message.includes("user validation failed")) {
                // console.log(err);
                Object.values(err.errors).forEach(({ properties }) => {
                        // console.log(val);
                        // console.log(properties);
                        errors[properties.path] = properties.message;
                });
        }
        return errors;
};
// create json web token
const maxAge = 3 * 24 * 60 * 60;
const createToken = (id) => {
        return jwt.sign({ id }, "net ninja secret", {
                expiresIn: maxAge,
        });
};
// controller actions
module.exports.login_get = (req, res) => {
        res.render("login");
};

module.exports.login_post = async (req, res) => {
        const { email, password } = req.body;
        try {
                const user = await User.create({ email, password });
                const token = createToken(user._id);
                res.cookie("jwt", token, {
                        httpOnly: true,
                        maxAge: maxAge * 1000,
                });
                res.status(201).json({ user: user._id });
        } catch (err) {
                const errors = handleErrors(err);
                res.status(400).json({ errors });
        }
};