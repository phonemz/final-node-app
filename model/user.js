const mongoose = require("mongoose");
const { isEmail } = require("validator");
const bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema({
        {
                name: {
                        type: String,
                        required: true,
                        unique: true,
                        lowercase:true
                },
                password: {
                        type: String,
                        required:true
                }
    }
    
});
