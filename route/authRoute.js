const express = require("express");
const mongoose = require("mongoose");

const app = express();

const { Router } = require("express");
const authController = require("../controller/authController");
const router = Router();

app.get("/login", (req, res) => res.render("login"));

module.exports = router;
